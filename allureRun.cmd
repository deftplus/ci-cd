@chcp 65001

call allure generate --clean .\build\allure .\build\smoke\allure .\build\syntax-check\allure -o .\build\allure\allure-report 
&& allure open .\build\allure\allure-report

# Для PowerShell: allure generate --clean .\build\allure .\build\smoke\allure .\build\syntax-check\allure -o .\build\allure\allure-report; allure open .\build\allure\allure-report
